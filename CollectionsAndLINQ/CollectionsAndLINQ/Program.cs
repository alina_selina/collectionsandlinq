﻿using CollectionsAndLINQ.Models;
using CollectionsAndLINQ.Services;
using System;
using System.Collections.Generic;

namespace CollectionsAndLINQ
{
    class Program
    {
        private static ConsoleManager _consoleManager;
        private static Dictionary<int, Operation> _mainMenu;
        private static string _header = "QUERY SERVICE";

        static Program()
        {
            var dataManager = new DataManager();
            var projects = dataManager.PrepareData().Result;
            var projectService = new ProjectService(projects);
            _consoleManager = new ConsoleManager(projectService);

            InitializeMenu();
        }

        static void Main()
        {
            StartMenu(_mainMenu, _header);
        }

        static void StartMenu(Dictionary<int, Operation> menu, string header)
        {
            while (true)
            {
                Console.Clear();
                Menu.ShowHeader(header);
                Menu.Show(menu);
                Menu.SelectOperation(menu);
            }
        }

        static void InitializeMenu()
        {
            _mainMenu = new Dictionary<int, Operation>
            {
                {1, new Operation("ShowProjectTaskCountByUserId", new Action(()=>_consoleManager.ShowProjectTaskCountByUserId()))},
                {2, new Operation("ShowUserTasksByUserId", new Action(()=>_consoleManager.ShowUserTasksByUserId()))},
                {3, new Operation("ShowUserTasksCurrentYearByUserId", new Action(()=>_consoleManager.ShowUserTasksCurrentYearByUserId()))},
                {4, new Operation("ShowTeams", new Action(()=>_consoleManager.ShowTeams()))},
                {5, new Operation("ShowUsers", new Action(()=>_consoleManager.ShowUsers()))},
                {6, new Operation("ShowUserStruct", new Action(()=>_consoleManager.ShowUserStruct()))},
                {7, new Operation("ShowProjectStruct", new Action(()=>_consoleManager.ShowProjectStruct()))},
                {8, new Operation("Quit", new Action(()=>Environment.Exit(0))) }
            };
        }
    }
}