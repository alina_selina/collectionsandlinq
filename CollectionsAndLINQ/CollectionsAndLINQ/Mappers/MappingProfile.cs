﻿using AutoMapper;
using CollectionsAndLINQ.Entities;
using CollectionsAndLINQ.Models;
using System.Collections.Generic;

namespace CollectionsAndLINQ.MappingProfiles
{
    public class MappingProfile
    {
        public IMapper Mapper { get; private set; }

        public MappingProfile()
        {
            var config = GetConfig();
            Mapper = config.CreateMapper();
        }

        private MapperConfiguration GetConfig()
        {
            return new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<UserDTO, User>();

                cfg.CreateMap<(TaskDTO, User), Task>()
                .ForMember(d => d.CreatedAt, opt => opt.MapFrom(s => s.Item1.CreatedAt))
                .ForMember(d => d.Description, opt => opt.MapFrom(s => s.Item1.Description))
                .ForMember(d => d.FinishedAt, opt => opt.MapFrom(s => s.Item1.FinishedAt))
                .ForMember(d => d.Id, opt => opt.MapFrom(s => s.Item1.Id))
                .ForMember(d => d.Name, opt => opt.MapFrom(s => s.Item1.Name))
                .ForMember(d => d.ProjectId, opt => opt.MapFrom(s => s.Item1.ProjectId))
                .ForMember(d => d.State, opt => opt.MapFrom(s => s.Item1.State))
                .ForMember(d => d.Performer, opt => opt.MapFrom(s => s.Item2));

                cfg.CreateMap<(TeamDTO, List<User>), Team>()
                .ForMember(d => d.Id, opt => opt.MapFrom(s => s.Item1.Id))
                .ForMember(d => d.Name, opt => opt.MapFrom(s => s.Item1.Name))
                .ForMember(d => d.CreatedAt, opt => opt.MapFrom(s => s.Item1.CreatedAt))
                .ForMember(d => d.Users, opt => opt.MapFrom(s => s.Item2));
            });
        }
    }
}
