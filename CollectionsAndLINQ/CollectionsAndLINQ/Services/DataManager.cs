﻿using AutoMapper;
using CollectionsAndLINQ.Entities;
using CollectionsAndLINQ.MappingProfiles;
using CollectionsAndLINQ.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace CollectionsAndLINQ.Services
{
    public class DataManager
    {
        private readonly IMapper _mapper;

        public DataManager()
        {
            _mapper = new MappingProfile().Mapper;
        }
        public async Task<IEnumerable<Project>> PrepareData()
        {
            var data = await FetchData();
            var projects = CombineData(data);
            return projects;
        }

        private async Task<(List<UserDTO> users, List<TeamDTO> teams, List<TaskDTO> tasks, List<ProjectDTO> projects)> FetchData()
        {
            using var client = new HttpClient() { BaseAddress = new Uri(Settings.BaseAddress) };

            var users = client.GetStringAsync("users");
            var teams = client.GetStringAsync("teams");
            var tasks = client.GetStringAsync("tasks");
            var projects = client.GetStringAsync("projects");

            await System.Threading.Tasks.Task.WhenAll(new[] { users, teams, tasks, projects });

            var projectDTOs = JsonConvert.DeserializeObject<List<ProjectDTO>>(await projects);
            var teamDTOs = JsonConvert.DeserializeObject<List<TeamDTO>>(await teams);
            var taskDTOs = JsonConvert.DeserializeObject<List<TaskDTO>>(await tasks);
            var userDTOs = JsonConvert.DeserializeObject<List<UserDTO>>(await users);

            return (userDTOs, teamDTOs, taskDTOs, projectDTOs);
        }

        private IEnumerable<Project> CombineData((List<UserDTO> users, List<TeamDTO> teams, List<TaskDTO> tasks, List<ProjectDTO> projects) data)
        {
            var projects = from pr in data.projects
                           join tms in from tmss in data.teams
                                       join us in data.users on tmss.Id equals us.TeamId into uss
                                       let team = _mapper.Map<(TeamDTO, List<User>), Team>((tmss, _mapper.Map<List<UserDTO>, List<User>>(new List<UserDTO>(uss))))
                                       select team on pr.TeamId equals tms.Id
                           join au in data.users on pr.AuthorId equals au.Id
                           join ts in from tss in data.tasks
                                      join ps in data.users on tss.PerformerId equals ps.Id
                                      let task = _mapper.Map<(TaskDTO, User), Entities.Task>((tss, _mapper.Map<UserDTO, User>(ps)))
                                      select task on pr.Id equals ts.ProjectId into tsks
                           select new Project()
                           {
                               Author = _mapper.Map<UserDTO, User>(au),
                               CreatedAt = pr.CreatedAt,
                               Deadline = pr.Deadline,
                               Description = pr.Description,
                               Id = pr.Id,
                               Name = pr.Name,
                               Team = tms,
                               Tasks = new List<Entities.Task>(tsks)
                           };

            return projects;
        }
    }
}
