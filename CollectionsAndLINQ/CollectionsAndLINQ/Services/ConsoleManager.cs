﻿using System;
using System.Collections.Generic;
using System.Linq;
using CollectionsAndLINQ.Entities;
using static System.Console;
using static CollectionsAndLINQ.Utils.ConsoleUtils;

namespace CollectionsAndLINQ.Services
{
    public class ConsoleManager
    {
        private readonly int _pageSize = 20;
        private readonly ProjectService _projectService;

        public ConsoleManager(ProjectService projectService)
        {
            _projectService = projectService;
        }

        public void ShowProjectTaskCountByUserId()
        {
            if (!TryGetIdInput(out int id))
                return;

            var result = _projectService.GetProjectTaskCountByUserId(id);

            if (result?.Count > 0)
                foreach (KeyValuePair<Project, int> keyValue in result)
                    WriteLine($"Project: id - {keyValue.Key.Id}, name - {keyValue.Key.Name}, taskCount - {keyValue.Value}");
            else
                WriteLine("Not yet");

            WaitForAnyKeyPress();
        }

        public void ShowUserTasksByUserId()
        {
            if (!TryGetIdInput(out int id))
                return;

            var result = _projectService.GetUserTasksByUserId(id);

            if (result?.Count > 0)
                result.ForEach(t => WriteLine(t.ToString()));
            else
                WriteLine("Not yet");

            WaitForAnyKeyPress();
        }

        public void ShowUserTasksCurrentYearByUserId()
        {
            if (!TryGetIdInput(out int id))
                return;

            var result = _projectService.GetUserTasksCurrentYearByUserId(id);

            if (result?.Count > 0)
                result.ForEach(t => WriteLine($"id - {t.id}, name - {t.name}"));
            else
                WriteLine("Not yet");

            WaitForAnyKeyPress();
        }

        public void ShowTeams()
        {
            var result = _projectService.GetTeams();

            if (result?.Count > 0)
                foreach ((int? id, string teamName, List<User> users) tuple in result)
                {
                    WriteLine($"\nid - {tuple.id}, name - {tuple.teamName}, \nusers:");
                    tuple.users.ForEach(u => WriteLine(u.ToString()));
                }
            else
                WriteLine("Not yet");

            WaitForAnyKeyPress();
        }

        public void ShowUsers()
        {
            int count = 0;
            var result = _projectService.GetUsers();

            if (result?.Count > 0)
                while (true)
                {
                    Clear();
                    foreach ((User user, List<Task> tasks) tuple in result.Skip(count).Take(_pageSize))
                    {
                        WriteLine($"\n{tuple.user}");
                        tuple.tasks.ForEach(t => WriteLine(t.ToString()));
                    }

                    if (!TryGetNextCount(ref count, _pageSize, result.Count))
                        break;
                }
            else
                WriteLine("Not yet");

            WaitForAnyKeyPress();
        }

        public void ShowUserStruct()
        {
            if (!TryGetIdInput(out int id))
                return;

            var result = _projectService.GetUserStruct(id);

            WriteLine($"user: {result.user}\nlastProject: {result.lastProject}\nlastProjectTaskCount: {result.lastProjectTaskCount}\n" +
                $"unfinishedTaskCount: {result.unfinishedTaskCount}\nlongestTask: {result.longestTask}");

            WaitForAnyKeyPress();
        }

        public void ShowProjectStruct()
        {
            int count = 0;
            var result = _projectService.GetProjectStruct();

            if (result?.Count > 0)
                while (true)
                {
                    Clear();
                    foreach ((Project project, Task longestTask, Task shortestTask, int userCount) in result.Skip(count).Take(_pageSize))
                        WriteLine($"{project} | longestTask: {longestTask} |  shortestTask: {shortestTask} | userCount: {userCount}\n");

                    if (!TryGetNextCount(ref count, _pageSize, result.Count))
                        break;
                }
            else
                WriteLine("Not yet");

            WaitForAnyKeyPress();
        }

        private static bool TryGetNextCount(ref int count, int page, int maxCount)
        {
            WriteWarningMessage($"prev page [p] | next page [any key] | quit [q]: ");
            ConsoleKeyInfo key = ReadKey();

            if (key.KeyChar == 'q')
                return false;
            else if (key.KeyChar == 'p')
                count -= count > 0 ? page : 0;
            else
                count += count + page > maxCount ? 0 : page;

            return true;
        }

        private static bool TryGetIdInput(out int id)
        {
            WriteLine("\nEnter id:");

            while (true)
            {
                if (int.TryParse(ReadLine(), out id))
                    return true;

                else if (!IsTryingGetValueAgain("Incorrect value."))
                {
                    id = 0;
                    return false;
                }
            }
        }
    }
}
