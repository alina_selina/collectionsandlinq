﻿using System;

namespace CollectionsAndLINQ.Models
{
    public class ProjectDTO
    {
        public int Id { get; set; }
        public int AuthorId { get; set; }
        public int TeamId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public DateTime Deadline { get; set; }
        public DateTime CreatedAt { get; set; }

        public override string ToString()
        {
            return $"Project id: {Id}, AuthorId: {AuthorId}, TeamId: {TeamId}, Name: {Name}, Deadline: {Deadline}, CreatedAt: {CreatedAt}";
        }
    }
}
