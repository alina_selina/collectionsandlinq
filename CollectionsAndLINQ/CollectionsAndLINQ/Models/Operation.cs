﻿using System;

namespace CollectionsAndLINQ.Models
{
    public struct Operation
    {
        public string Name { get; set; }
        public Action Command { get; set; }

        public Operation(string name, Action method)
        {
            Name = name;
            Command = method;
        }
    }
}
