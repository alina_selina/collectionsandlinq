﻿using System;

namespace CollectionsAndLINQ
{
    public static class Settings
    {
        public const ConsoleColor ErrorMessageColor = ConsoleColor.Red;
        public const ConsoleColor WarningMessageColor = ConsoleColor.Yellow;

        public const string BaseAddress = "https://bsa21.azurewebsites.net/api/";
    }
}
