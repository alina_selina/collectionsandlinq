﻿using CollectionsAndLINQ.Enums;
using System;

namespace CollectionsAndLINQ.Entities
{
    public class Task : BaseEntity
    {
        public int ProjectId { get; set; }
        public User Performer { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public TaskState State { get; set; }
        public DateTime? FinishedAt { get; set; }

        public Task() { }

        public override string ToString()
        {
            return $"Task id: {Id}, ProjectId: {ProjectId}, PerformerId: {Performer.Id}, Name: {Name}, State: {State}, CreatedAt: {CreatedAt}";
        }
    }
}
