﻿using System;

namespace CollectionsAndLINQ.Entities
{
    public abstract class BaseEntity
    {
        private DateTime _createdAt;

        public BaseEntity()
        {
            CreatedAt = DateTime.Now;
        }

        public int Id { get; set; }
        public DateTime CreatedAt
        {
            get => _createdAt;
            set => _createdAt = (value == DateTime.MinValue) ? DateTime.Now : value;
        }
    }
}
