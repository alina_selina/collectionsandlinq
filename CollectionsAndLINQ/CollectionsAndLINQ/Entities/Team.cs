﻿using System.Collections.Generic;

namespace CollectionsAndLINQ.Entities
{
    public class Team : BaseEntity
    {
        public string Name { get; set; }
        public ICollection<User> Users { get; set; }

        public override string ToString()
        {
            return $"Team id: {Id}, Name: {Name}, CreatedAt: {CreatedAt}";
        }
    }
}
