﻿using System;
using System.Collections.Generic;

namespace CollectionsAndLINQ.Entities
{
    public class Project:BaseEntity
    {
        public ICollection<Task> Tasks { get; set; }
        public User Author { get; set; }
        public Team Team { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public DateTime Deadline { get; set; }

        public override string ToString()
        {
            return $"Project id: {Id}, AuthorId: {Author.Id}, TeamId: {Team.Id}, Name: {Name}, Deadline: {Deadline}, CreatedAt: {CreatedAt}," +
                $"tasks: {Tasks.Count}";
        }
    }
}
