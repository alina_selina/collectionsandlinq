﻿using System;
using static System.Console;

namespace CollectionsAndLINQ.Utils
{
    public static class ConsoleUtils
    {
        public static void WaitForAnyKeyPress(string message = "\nPress any key to continue...")
        {
            WriteLine(message);
            ReadKey();
        }

        public static void ClearLine()
        {
            SetCursorPosition(0, CursorTop - 1);
            Write(new string(' ', WindowWidth));
            SetCursorPosition(0, CursorTop - 1);
        }

        public static bool IsTryingGetValueAgain(string message)
        {
            while (true)
            {
                ClearLine();
                WriteWarningMessage($"{message} Try again? [y/n]: ");

                var input = ReadLine();
                if (input == "y")
                {
                    ClearLine();
                    return true;
                }
                else if (input == "n")
                {
                    ClearLine();
                    return false;
                }
            }
        }

        public static void WriteWarningMessage(string message, bool newLine = false)
        {
            WriteMessage(message, Settings.WarningMessageColor, newLine);
        }

        public static void WriteErrorMessage(string message, bool newLine = false)
        {
            WriteMessage(message, Settings.ErrorMessageColor, newLine);
        }

        public static void WriteMessage(string message, ConsoleColor color, bool newLine = false)
        {
            ForegroundColor = color;
            if (newLine)
                WriteLine(message);
            else
                Write(message);
            ResetColor();
        }
    }
}
